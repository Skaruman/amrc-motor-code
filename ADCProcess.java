import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;
import java.util.*;

public class ADCProcess {

	
	
	public static double readADCAnalogVoltage(int channel, ADC adc) throws InterruptedException {
		GpioPinDigitalOutput adcChipEnablePin = adc.getadcChipEnablePin();
		GpioPinDigitalOutput adcClockPin = adc.getadcClockPin();
		GpioPinDigitalOutput adcDataOutPin = adc.getadcDataOutPin();
		GpioPinDigitalInput adcDataInPin = adc.getadcDataInPin();
		int[] word = new int[7];
		//Set Respective Channels
		if (channel == 1) 
		{
			word[0] = 1; word[1] = 1; word[2] = 0; word[3] = 0;	word[4] = 0; word[5] = 1; word[6] = 1;
		}
		else if (channel == 2) 
		{
			word[0] = 1; word[1] = 1; word[2] = 0; word[3] = 0; word[4] = 1; word[5] = 1; word[6] = 1;
		}
		else if (channel == 3) 
		{
			word[0] = 1; word[1] = 1; word[2] = 1; word[3] = 1; word[4] = 1; word[5] = 1; word[6] = 1;
		}
		else if (channel == 4) 
		{
			word[0] = 1; word[1] = 1; word[2] = 1; word[3] = 1; word[4] = 1; word[5] = 1; word[6] = 1;
		}
		else if (channel == 5) 
		{
			word[0] = 1; word[1] = 1; word[2] = 1; word[3] = 1; word[4] = 1; word[5] = 1; word[6] = 1;
		}
		else if (channel == 6) 
		{
			word[0] = 1; word[1] = 1; word[2] = 1; word[3] = 1; word[4] = 1; word[5] = 1; word[6] = 1;
		}
		else if (channel == 7) 
		{
			word[0] = 1; word[1] = 1; word[2] = 1; word[3] = 1; word[4] = 1; word[5] = 1; word[6] = 1;
		}
		adcChipEnablePin.low(); //enable chip
		double anip = 0; 		//clear variable
		//clock out 7 bits to select channel
		for (int i = 0; i < 7; i++) {
			if (word[i] == 0) {
				adcDataOutPin.low();
			}
			else if (word[1] == 1) {
				adcDataOutPin.high();
			}
			Thread.sleep(10);
			adcClockPin.high();
			Thread.sleep(10);
			adcClockPin.low();
		}
		//clock in 11 bits of data
		for (int i = 0; i < 12; i++) {
			adcClockPin.high();    							//set clock high
			Thread.sleep(10);
			int bit = adcDataInPin.getState().getValue();   //read input
			Thread.sleep(10);
			adcClockPin.low();  							//set clock low
			double value = Math.pow((bit*2),(12-i-1));  	//work out value of this bit
			anip = anip + value;         					//add to previous total
		}
		adcChipEnablePin.high();      			//disable chip
		double halfVolt = anip * (2.5/4096); 	//use ref voltage of 2.5 to work out voltage
		double volt = halfVolt * 2; 			//As the ADC can only take 2.5V max input, we manually divide the voltage in two using resistors, and here we multiply it back up again
		return volt;
	}
	
	
	
	
	public static double getRMS(int channel, long duration, double rZero, ADC adc) throws InterruptedException {
		double rVal = 0; 
		int sampleCount = 0;
		double rSquaredSum = 0;
		long start_time = System.currentTimeMillis();
		long durationMS = duration * 1000;
		while ((System.currentTimeMillis()) < (start_time + durationMS)) {
			rVal = readADCAnalogVoltage(channel, adc);
			rVal = rVal - rZero;
			rSquaredSum = rSquaredSum + (rVal * rVal);
			sampleCount++;
		}
		double RMS = Math.sqrt(rSquaredSum/sampleCount);
		return RMS;
	}
	
	
	
	
	public static double getCurrentOrVoltage(double mVperUnit, int channel, long duration, double zeroValue, double rNoise, ADC adc) throws InterruptedException {
		double valueVRMS = getRMS(channel, duration, zeroValue, adc);
		double valueUnitRMS = (valueVRMS * 1000)/mVperUnit; 	//Convert to Amps/Volts
		//As this particular method involves attempting to calculate a sine wave signal from random values,
		//if you pass a steady DC value, (eg. when the relay is switched off, the sensors output VCC/2, aka 2.5V),
		//then the method of squaring, averaging and square rooting the values will result in a false reading close to zero.
		//Therefore if the value is too close to this limit, we deem the true reading to be zero.
		if (valueUnitRMS < rNoise)
		{
			valueUnitRMS = 0.0;		
		}
		return valueUnitRMS;
	}
}