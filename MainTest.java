import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;
import java.util.*;

public class MainTest {

   	//Global User Inputs//////////////////////////////////////////////////////////////////////////////////////
   	public static int durationRMS = 3;					//loop length per RMS polling in seconds.
   	public static int calibrationTime = 10;				//time spent calculating a zero average for calibration in seconds.
	public static int toggleCycle = 6;					//number of display cycles before the relay toggles on or off.
	public static int cyclesUntilEnd = 100;				//number of display cycles until program ends.
	public static int currentChannel = 1;				//visual number of the ADC port which the current sensor wire is connected to.
	public static int voltageChannel = 2;				//visual number of the ADC port which the voltage sensor wire is connected to.
	public static double currentmVperAmp = 185*(2/2.5); //depends on ACS712 version. (Use 185 for 5A, use 100 for 20A Module and 66 for 30A Module). Also the sensor was consistently reading 2Amps when it should have been 2.5Amp, so we added the multiplier
	public static double voltagemVperVolt = 1.2;		//This value was found by using a multimeter and calculating what the value should be.
	public static double currentNoiseLimit = 0.23;	 	//This counteracts the small noise around 0.0 that creates a false RMS reading. Anything under this will be ignored.		
	public static double voltageNoiseLimit = 10.0;		//This counteracts the small noise around 0.0 that creates a false RMS reading. Anything under this will be ignored.
    public static final GpioController gpio = GpioFactory.getInstance(); // Create gpio controller and pins
	public static final GpioPinDigitalOutput relayPin = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_00, "relay", PinState.LOW);
	public static final GpioPinDigitalOutput adcChipEnablePin = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_10, "adcChipEnable", PinState.HIGH);
	public static final GpioPinDigitalOutput adcClockPin = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_14, "adcClock", PinState.LOW);
	public static final GpioPinDigitalOutput adcDataOutPin = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_12, "adcDataOut", PinState.HIGH);
	public static final GpioPinDigitalInput adcDataInPin = gpio.provisionDigitalInputPin(RaspiPin.GPIO_13, "adcDataIn");
	public static final ADC adc = new ADC(adcChipEnablePin, adcClockPin, adcDataOutPin, adcDataInPin);
	//////////////////////////////////////////////////////////////////////////////////////////////////////////

	
	public static void main(String[] args) throws InterruptedException {
	
		//Initialise Values///////////////////////////////////////////////////////////////////////////////////////
		float temp = 0;
		double current = 0;
		double voltage = 0;
        relayPin.setShutdownOptions(true, PinState.LOW);
		adcChipEnablePin.setShutdownOptions(true, PinState.LOW);
		adcClockPin.setShutdownOptions(true, PinState.LOW);
		adcDataOutPin.setShutdownOptions(true, PinState.LOW);
		adcDataInPin.setShutdownOptions(true);
		System.out.println("----------------------------------------------------------------------------------------------");
		System.out.println("--> MainTest.java Started");
		System.out.println("----------------------------------------------------------------------------------------------");
		//End of Initialise Values////////////////////////////////////////////////////////////////////////////////


		
		//Begin Calibrate ///////////////////////////////////////////////////////////////////////////////////////
		double zeroCurrent = 0;
		double zeroVoltage = 0;
		int count = 1;
		System.out.println("--> Beginning Calibration for " + (calibrationTime*2) + " seconds");
		System.out.println("----------------------------------------------------------------------------------------------");
		relayPin.low();
        System.out.println("--> Relay OFF");
        Thread.sleep(5000);	//Ensure Relay is off before calibrating
		zeroCurrent = ADCProcess.getRMS(currentChannel, calibrationTime, 0.0, adc);
		zeroVoltage = ADCProcess.getRMS(voltageChannel, calibrationTime, 0.0, adc);
		System.out.println("--> Ending Calibration");
		System.out.println("----------------------------------------------------------------------------------------------");
		//End Calibrate ///////////////////////////////////////////////////////////////////////////////////////
		
		
		//Begin of Display Loop////////////////////////////////////////////////////////////////////////////////
		count = 1;
		relayPin.high();
		System.out.println("--> Relay ON");
		System.out.println("----------------------------------------------------------------------------------------------");
		while (count < cyclesUntilEnd) {
			temp = Temperature.readTemp();
			current = ADCProcess.getCurrentOrVoltage(currentmVperAmp, currentChannel, durationRMS, zeroCurrent, currentNoiseLimit, adc);
			voltage = ADCProcess.getCurrentOrVoltage(voltagemVperVolt, voltageChannel, durationRMS, zeroVoltage, voltageNoiseLimit, adc);
			current = Math.round(current * 100.0) / 100.0;
			voltage = Math.round(voltage * 10.0) / 10.0;
			System.out.printf("%s %5.2f %s %3.2f %s %4.1f %n", "Temp = ", temp, "|| Current: ", current, "|| Voltage: ", voltage);
			if ((count % toggleCycle) == 0)
			{
				relayPin.toggle();
				System.out.println("----------------------------------------------------------------------------------------------");
				System.out.println("--> Relay toggled every " + toggleCycle + " cycles");
				System.out.println("----------------------------------------------------------------------------------------------");
			}
			count ++;
		}
        gpio.shutdown();
        System.out.println("--> Exiting MainTest.java");
		System.out.println("----------------------------------------------------------------------------------------------");
		//End of Display Loop//////////////////////////////////////////////////////////////////////////////////
    }
}
