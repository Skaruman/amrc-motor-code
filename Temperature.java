import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileReader;

public class Temperature {

	static String w1DirPath = "/sys/bus/w1/devices";
	
	public static float readTemp() {
		File dir = new File(w1DirPath);
        File[] files = dir.listFiles(new DirectoryFileFilter());
        float tempC = 0;
        if (files != null) {
            for(File file: files) {
              // Device data in w1_slave file
              String filePath = w1DirPath + "/" + file.getName() + "/w1_slave";
              File f = new File(filePath);
              try(BufferedReader br = new BufferedReader(new FileReader(f))) {
                String output;
                while((output = br.readLine()) != null) {
                  int idx = output.indexOf("t=");
                  if(idx > -1) {
                    // Temp data (multiplied by 1000) in 5 chars after t=
                    tempC = Float.parseFloat(output.substring(output.indexOf("t=") + 2));
                    // Divide by 1000 to get degrees Celsius
                    tempC /= 1000;
                  }
                }
              }
              catch(Exception ex) {
                System.out.println(ex.getMessage());
              }
            }
        }
        return tempC;
	}
}

// This FileFilter selects subdirs with name beginning with 28-
// Kernel module gives each 1-wire temp sensor name starting with 28-
class DirectoryFileFilter implements FileFilter {
	public boolean accept(File file) {
		String dirName = file.getName();
		String startOfName = dirName.substring(0, 3);
		return (file.isDirectory() && startOfName.equals("28-"));
	}
}
