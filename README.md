# README #

In order to compile this current simple code, it needs the "pi4j" libraries. (Link here: http://pi4j.com/example/control.html)

We will continue to look this project, and hopefully will easily implement it into the pre-existing motor-thing-new.

More information is found at the sharepoint: https://sites.uk.deloitte.com/cons/Industry4111090/Document Library/01. Internal/01. Knowledge Mgt and Internal Comms/03.01. Demo - PM/Portable Version

There is a powerpoint showing this information, as well as circuit diagrams, and a paper written by someone else who uses these exact same sensors.

# MORE INFORMATION ON THE SENSORS #

Solid State Relay (Part A)
Requirements: 240VAC Load – 5VDC Control.
There are many suitable relays available, we choose: http://amzn.to/2qGvShI (Spec: 24-480VAC Load - 3-32VDC Control).
As the 3.3V outputted from one of the RPi’s GPIO control pins could easily drop down below the 3VDC control needed, it’s safer to power the relay using the constant 5V rail, but using a transistor to control whether it’s open or closed.
For the logic of this setup we followed: http://bit.ly/2r8lGA0. This guide uses the other components:
NPN 2N2222A Transistor
1N4001 Protection Diode
1K Ohm Resistor

Temperature Sensor (Part B)
Requirements: 3.3-5V Power Supply, Up to 100°C, Small size to attach to motor housing.
Through research we found that the most common and easy temperature sensor to interface with the RPi, is a ‘one-wire’ digital sensor from DALLAS.
DS18B20 One Wire Digital Temperature Sensor: http://bit.ly/2qqYVH1 (Spec: 3.0-5.5V Power Supply, -55C to 125C (+/-0.5C))
In the setup we followed these guides: http://bit.ly/2qboGqz and http://bit.ly/2r1tss6. The setup requires a 4.7kOhm pull-up resistor accross the power and signal lines. This is explained here: http://bit.ly/2p1g7P6 
Note: The DS18B20 is available in two forms; a waterproof probe and a simple chip in a TO-92 housing. We choose the chip as it would be easier to stick to the motor case with thermal adhesive, and less unnecessarily insulated than the waterproof one.

Current Sensor (Part C)
Requirements: Measure 240VAC, Control 3.3-5VDC
ACS712: http://amzn.to/2pk4k2P (Spec: Measure 240AC, Control 5VDC. Datasheet: http://bit.ly/2q27tVh) 
This sensor outputs it’s signal as an analogue voltage, so as the RPi does not contain any analogue GPIO pins, this required an ADC converter. (see later)
As the measurement of alternating current is instantaneous, we have sample the value continually before calculating the correct current. We found that the usual way of calculating RMS from the Peak-to-Peak (found here: http://bit.ly/2rsxHQI), was too noisy and unreliable for our sensors. So we use an alternative more basic method of zeroing, squaring, and averaging the values to find the RMS. (http://bit.ly/2qTqxoU & http://bit.ly/2qXH2vk) (Other sensor setup guides: http://bit.ly/2q27rwD & http://bit.ly/2rJYnbV). 
Note: This method of generating a RMS, causes a false reading to be picked up based of the small amount of noise around the zero point (when the relay is off). Therefore we added some logic o ignore very small values. (this is the same for the voltage sensor).
Note: Although this sensor outputs an analogue signal voltage of 0V to 5V (with 2.5V corresponding to 0 Amps measured, >2.5V = a positive direction current, <2.5V = a negative direction current), we discovered that our specific Analogue-Digital Converter’s inputs were capped at 2.5V (see later). So we used a simple voltage divided circuit to split the voltage in half, reducing the range to 0-2.5V, with the zero midpoint reading at 1.25V. This requires two resistors of any value as long as they are equal: http://bit.ly/2r8nOHS. 

Voltage Sensor (Part D)
Requirements: Measure 240VAC, Control 3.3-5VDC
ZMPT101B: http://ebay.eu/2rKaq9b (Datasheet: http://bit.ly/2rCYtD)
This voltage sensor works identically to the above current sensor: It runs of 5V and it outputs signal analogue 0-5V (Zero reading midpoint 2.5V), thus it also needs a ADC and a voltage divider circuit.
Note: There are 2 GND pins on this board: not sure why?

Analogue to Digital Converter (ADC)
As mentioned earlier the current and voltage sensors output their measurement signal as analogue voltage, and as the RPi does not contain any analogue inputs, it was required to get an Analogue-to-Digital Converter (ADC). One ADC that we found that seemed simple to use was the Custard Pi 3. It fits over the top of the RPi and leaves the RPi’s GPIO pins free for the other components, using minimal pins itself.
Custard Pi 3: http://bit.ly/2q2ewNr (Guide including sample code: http://bit.ly/2rHQ4OF) 
Note: It’s mentioned that “Maximum analogue input is 2.5V per channel in single ended mode”. At the moment I don’t know if it’s possible to increase this maximum input to equal the sensor’s 5V, so we could avoid the voltage divider circuit and any accuracy that is lost as a result.

Vibration Sensor (MPU6050)
This component has already been decided, and as such it was not included in the previous diagrams, because it uses separate independent pins via I2C. (3.3V, SDA, SCL & GND)
MPU6050: http://amzn.to/2q2nBpy 
Guide: http://bit.ly/1PkeouI 