import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.GpioPinDigitalInput;

public class ADC {
        
	public GpioPinDigitalOutput adcChipEnablePin;
	public GpioPinDigitalOutput adcClockPin;
	public GpioPinDigitalOutput adcDataOutPin;
	public GpioPinDigitalInput adcDataInPin;
        
    public ADC(GpioPinDigitalOutput adcChipEnablePinIn, GpioPinDigitalOutput adcClockPinIn, GpioPinDigitalOutput adcDataOutPinIn, GpioPinDigitalInput adcDataInPinIn) {
        adcChipEnablePin = adcChipEnablePinIn;
        adcClockPin = adcClockPinIn;
        adcDataOutPin = adcDataOutPinIn;
        adcDataInPin = adcDataInPinIn;
    }
        
    public GpioPinDigitalOutput getadcChipEnablePin() {
        return adcChipEnablePin;
    }
        
    public void setadcChipEnablePin(GpioPinDigitalOutput pinIn) {
        adcChipEnablePin = pinIn;
    }
	
    public GpioPinDigitalOutput getadcClockPin() {
        return adcClockPin;
    }
        
    public void setadcClockPin(GpioPinDigitalOutput pinIn) {
        adcClockPin = pinIn;
    }
	
    public GpioPinDigitalOutput getadcDataOutPin() {
        return adcDataOutPin;
    }
        
    public void setadcDataOutPin(GpioPinDigitalOutput pinIn) {
        adcDataOutPin = pinIn;
    }
	
    public GpioPinDigitalInput getadcDataInPin() {
        return adcDataInPin;
    }
        
    public void setadcDataInPin(GpioPinDigitalInput pinIn) {
        adcDataInPin = pinIn;
    }
 
}